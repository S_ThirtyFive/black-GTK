# Black-Is-The-New-Black

Black GTK 2,3 theme. Forked from [Flat-Remix-GTK](https://github.com/daniruiz/Flat-Remix-GTK) theme. 

Almost complete black with a touch of gray and levery little can.

**Installation**

- Clone this repo
- unzip the file
- cd into the directory
- `sudo make install`

Screenshot:

![alt text](https://gitlab.com/S_ThirtyFive/black-GTK/-/raw/master/Black-Is-The-New-Black.png)